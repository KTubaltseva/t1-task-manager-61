package ru.t1.ktubaltseva.tm.exception.data;

import org.jetbrains.annotations.NotNull;

public class SqlDataException extends AbstractDataException {

    public SqlDataException(@NotNull final Throwable cause) {
        super(cause);
    }
}
