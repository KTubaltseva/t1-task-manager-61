package ru.t1.ktubaltseva.tm.exception.auth;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public abstract class AbstractAuthException extends AbstractException {

    public AbstractAuthException() {
    }

    public AbstractAuthException(@NotNull final String message) {
        super(message);
    }

    public AbstractAuthException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractAuthException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractAuthException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
