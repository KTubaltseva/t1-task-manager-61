package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectDTORepository extends UserOwnedDTORepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    List<ProjectDTO> findByUserId(
            @Param("userId") @NotNull String userId,
            @NotNull Sort sort
    );

    @Nullable
    ProjectDTO findByUserIdAndId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );


    void deleteByUserId(@Param("userId") @NotNull String userId);

    boolean existsByUserIdAndId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    long countByUserId(@Param("userId") @NotNull String userId);

    void deleteByUserIdAndId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

}
