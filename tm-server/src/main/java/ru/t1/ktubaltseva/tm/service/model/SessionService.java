package ru.t1.ktubaltseva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.ISessionService;
import ru.t1.ktubaltseva.tm.enumerated.TMSort;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.Session;
import ru.t1.ktubaltseva.tm.repository.model.SessionRepository;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session, SessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository repository;


    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        return repository.findByUserId(userId);
    }

    @NotNull
    @Override
    public Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session resultModel = repository.findByUserIdAndId(userId, id);
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public long getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        return repository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<Session> findAll(
            @Nullable final String userId,
            @Nullable final TMSort tmSort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @Nullable Sort sort;
        if (tmSort == null) sort = Sort.by(Sort.Direction.ASC, "created");
        else switch (tmSort.name()) {
            case "BY_NAME":
                sort = Sort.by(Sort.Direction.ASC, "name");
                break;
            case "BY_STATUS":
                sort = Sort.by(Sort.Direction.ASC, "status");
                break;
            default:
                sort = Sort.by(Sort.Direction.ASC, "created");
        }
        return repository.findByUserId(userId, sort);
    }

}
