package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Param("login") @NotNull String login);

    @Nullable
    UserDTO findByEmail(@Param("email") @NotNull String email);

    @NotNull
    Boolean existsByLogin(@Param("login") @NotNull String login);

    @NotNull
    Boolean existsByEmail(@Param("email") @NotNull String email);

}
