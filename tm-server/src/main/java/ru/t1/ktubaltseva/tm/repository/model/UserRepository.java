package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findByLogin(@Param("login") @NotNull String login);

    @Nullable
    User findByEmail(@Param("email") @NotNull String email);

    @NotNull
    Boolean existsByLogin(@Param("login") @NotNull String login);

    @NotNull
    Boolean existsByEmail(@Param("email") @NotNull String email);

}
