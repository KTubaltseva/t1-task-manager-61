package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends UserOwnedRepository<Task> {

    @NotNull
    List<Task> findByUserIdAndProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

    void deleteByUserIdAndProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId
    );

    @NotNull
    List<Task> findByUserId(
            @Param("userId") @NotNull String userId,
            @NotNull Sort sort
    );

    @NotNull
    List<Task> findByUserId(@Param("userId") @NotNull String userId);

    @Nullable
    Task findByUserIdAndId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );


    void deleteByUserId(@Param("userId") @NotNull String userId);

    boolean existsByUserIdAndId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    long countByUserId(@Param("userId") @NotNull String userId);

    void deleteByUserIdAndId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

}
