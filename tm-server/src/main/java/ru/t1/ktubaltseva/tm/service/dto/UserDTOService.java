package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ISessionDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.EmailAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.RoleEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.UserDTORepository;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserDTOService extends AbstractDTOService<UserDTO, UserDTORepository> implements IUserDTOService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private UserDTORepository repository;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ISessionDTOService sessionService;

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return repository.saveAndFlush(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (isEmailExists(login)) throw new EmailAlreadyExistsException(email);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return repository.saveAndFlush(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setRole(role);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return repository.saveAndFlush(user);
    }

    @NotNull
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = repository.findByLogin(login);
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDTO resultUser = repository.findByEmail(email);
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.existsByLogin(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.existsByEmail(email);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    @Transactional
    public void remove(@Nullable final UserDTO model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        taskService.clear(model.getId());
        projectService.clear(model.getId());
        sessionService.clear(model.getId());
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        @Nullable UserDTO resultModel = findByLogin(login);
        remove(resultModel);
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        @Nullable UserDTO resultModel = findByEmail(email);
        remove(resultModel);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable UserDTO resultUser = findOneById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        return update(resultUser);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

    @Override
    @Transactional
    public @NotNull UserDTO update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable String lastName
    ) throws AbstractException {
        @NotNull final UserDTO user = findOneById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return update(user);
    }

}
