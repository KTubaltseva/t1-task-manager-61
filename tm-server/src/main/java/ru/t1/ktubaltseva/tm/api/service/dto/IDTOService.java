package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.AbstractModelDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.Collection;
import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> {

    @NotNull
    M add(@Nullable M model) throws AbstractException;

    @NotNull
    Collection<M> add(@Nullable Collection<M> models) throws AbstractException;

    void clear() throws AbstractException;

    boolean existsById(@Nullable String id) throws AbstractException;

    @NotNull
    List<M> findAll() throws AbstractException;

    @NotNull
    M findOneById(@Nullable String id) throws AbstractException;

    long getSize() throws AbstractException;

    void remove(@Nullable M model) throws AbstractException;

    void removeById(@Nullable String id) throws AbstractException;

    @NotNull
    Collection<M> set(@Nullable Collection<M> models) throws AbstractException;

    @NotNull
    M update(@Nullable M model) throws AbstractException;

}
