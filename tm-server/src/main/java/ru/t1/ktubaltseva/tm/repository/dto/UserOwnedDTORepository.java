package ru.t1.ktubaltseva.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
public interface UserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> {

}
