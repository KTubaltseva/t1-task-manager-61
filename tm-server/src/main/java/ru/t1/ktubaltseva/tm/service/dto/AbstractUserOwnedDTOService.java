package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.repository.dto.UserOwnedDTORepository;

import java.util.Collection;
import java.util.Collections;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO, R extends UserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

    @NotNull
    @Autowired
    private R repository;

    @NotNull
    @Override
    @Transactional
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return repository.saveAndFlush(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(
            @Nullable final String userId,
            @Nullable final Collection<M> models
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (models == null || models.isEmpty()) return Collections.emptyList();
        for (@NotNull final M model : models) {
            model.setUserId(userId);
        }
        return repository.saveAll(models);
    }

    @Override
    @Transactional
    public void remove(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        removeById(userId, model.getId());
    }

    @NotNull
    @Override
    @Transactional
    public M update(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(userId, model.getId())) throw new EntityNotFoundException();
        model.setUserId(userId);
        return update(model);
    }

}
