package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserOwnedWBSDTOService;
import ru.t1.ktubaltseva.tm.dto.model.AbstractUserOwnedModelWBSDTO;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.field.DescriptionEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.StatusEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.UserOwnedDTORepository;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnedWBSDTOService<M extends AbstractUserOwnedModelWBSDTO, R extends UserOwnedDTORepository<M>>
        extends AbstractUserOwnedDTOService<M, R> implements IUserOwnedWBSDTOService<M> {

    @NotNull
    @Autowired
    private R repository;

    @NotNull
    @Override
    @Transactional
    public M changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable M resultProject = findOneById(userId, id);
        resultProject.setStatus(status);
        resultProject = update(resultProject);
        return resultProject;
    }

    @NotNull
    @Override
    @Transactional
    public M update(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();

        @Nullable M model = findOneById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

}
