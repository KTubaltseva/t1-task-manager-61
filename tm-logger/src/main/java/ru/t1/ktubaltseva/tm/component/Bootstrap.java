package ru.t1.ktubaltseva.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.listener.EntityListener;
import ru.t1.ktubaltseva.tm.service.ReceiverService;

import javax.jms.JMSException;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    public void start() throws JMSException {
        receiverService.receive(entityListener);
        System.out.println("** LOGGER STARTED **");
    }

}
