package ru.t1.ktubaltseva.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['mongo.port']}")
    public Integer mongoPort;

    @NotNull
    @Value("#{environment['mongo.host']}")
    public String mongoHost;

    @NotNull
    @Value("#{environment['mongo.db']}")
    public String mongoDBName;

}
