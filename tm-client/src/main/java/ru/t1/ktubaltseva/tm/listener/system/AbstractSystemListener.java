package ru.t1.ktubaltseva.tm.listener.system;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Nullable
    @Autowired
    protected AbstractListener[] listeners;

    @Nullable
    @Autowired
    protected AbstractSystemListener[] systemListeners;


}