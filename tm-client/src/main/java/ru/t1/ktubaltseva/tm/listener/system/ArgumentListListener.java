package ru.t1.ktubaltseva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.listener.AbstractListener;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    private final String NAME = "argument-list";

    @NotNull
    private final String DESC = "Display argument list.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-al";
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@argumentListListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {

        for (@NotNull final AbstractListener listener : systemListeners) {
            @Nullable final String argument = listener.getArgument();
            System.out.println(argument);
        }
    }

}
