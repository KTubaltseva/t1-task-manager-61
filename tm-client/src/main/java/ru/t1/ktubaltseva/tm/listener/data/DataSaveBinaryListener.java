package ru.t1.ktubaltseva.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveBinaryRequest;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataSaveBinaryListener extends AbstractDataListener {

    @NotNull
    private final String NAME = "data-save-binary";

    @NotNull
    private final String DESC = "Save data to binary file.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@dataSaveBinaryListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SAVE BINARY DATA]");
        @NotNull final DataSaveBinaryRequest request = new DataSaveBinaryRequest(getToken());
        getDomainEndpoint().saveDataBinary(request);
    }

}
