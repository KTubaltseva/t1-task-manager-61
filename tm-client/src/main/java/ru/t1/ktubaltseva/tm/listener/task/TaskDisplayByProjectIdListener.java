package ru.t1.ktubaltseva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskDisplayByProjectIdRequest;
import ru.t1.ktubaltseva.tm.dto.response.task.TaskDisplayByProjectIdResponse;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskDisplayByProjectIdListener extends AbstractTaskListener {

    @NotNull
    private final String NAME = "task-display-by-project-id";

    @NotNull
    private final String DESC = "Display task list by project id.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@taskDisplayByProjectIdListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DISPLAY TASKS BY PROJECT ID]");
        System.out.println("[ENTER ID]:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskDisplayByProjectIdRequest request = new TaskDisplayByProjectIdRequest(getToken(), projectId);
        @NotNull final TaskDisplayByProjectIdResponse response = getTaskEndpoint().getTasksByProjectId(request);
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        renderTasks(tasks);
    }

}
