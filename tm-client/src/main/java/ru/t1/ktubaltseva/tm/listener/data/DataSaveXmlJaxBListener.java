package ru.t1.ktubaltseva.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveXmlJaxBRequest;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataSaveXmlJaxBListener extends AbstractDataListener {

    @NotNull
    private final String NAME = "data-save-xml-jaxb";

    @NotNull
    private final String DESC = "Save data to xml file.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@dataSaveXmlJaxBListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SAVE XML DATA]");
        @NotNull final DataSaveXmlJaxBRequest request = new DataSaveXmlJaxBRequest(getToken());
        getDomainEndpoint().saveDataXmlJaxB(request);
    }

}
