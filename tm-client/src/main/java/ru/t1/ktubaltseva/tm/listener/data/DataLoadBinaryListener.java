package ru.t1.ktubaltseva.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadBinaryRequest;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataLoadBinaryListener extends AbstractDataListener {

    @NotNull
    private final String NAME = "data-load-binary";

    @NotNull
    private final String DESC = "Load data from binary file.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@dataLoadBinaryListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[LOAD BINARY DATA]");
        @NotNull final DataLoadBinaryRequest request = new DataLoadBinaryRequest(getToken());
        getDomainEndpoint().loadDataBinary(request);
    }

}
