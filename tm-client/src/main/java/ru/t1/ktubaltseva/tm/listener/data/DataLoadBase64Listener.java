package ru.t1.ktubaltseva.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadBase64Request;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataLoadBase64Listener extends AbstractDataListener {

    @NotNull
    private final String NAME = "data-load-base64";

    @NotNull
    private final String DESC = "Load data from base64 file.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@dataLoadBase64Listener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[LOAD BASE64 DATA]");
        @NotNull final DataLoadBase64Request request = new DataLoadBase64Request(getToken());
        getDomainEndpoint().loadDataBase64(request);
    }

}
