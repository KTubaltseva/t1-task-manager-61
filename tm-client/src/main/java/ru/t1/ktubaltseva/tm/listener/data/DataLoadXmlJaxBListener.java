package ru.t1.ktubaltseva.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadXmlJaxBRequest;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataLoadXmlJaxBListener extends AbstractDataListener {

    @NotNull
    private final String NAME = "data-load-xml-jaxb";

    @NotNull
    private final String DESC = "Load data from xml file.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@dataLoadXmlJaxBListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[LOAD XML DATA]");
        @NotNull final DataLoadXmlJaxBRequest request = new DataLoadXmlJaxBRequest(getToken());
        getDomainEndpoint().loadDataXmlJaxB(request);
    }

}
