package ru.t1.ktubaltseva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectDisplayListRequest;
import ru.t1.ktubaltseva.tm.dto.response.project.ProjectDisplayListResponse;
import ru.t1.ktubaltseva.tm.enumerated.TMSort;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectDisplayListListener extends AbstractProjectListener {

    @NotNull
    private final String NAME = "project-list";

    @NotNull
    private final String DESC = "Display project list.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@projectDisplayListListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DISPLAY PROJECTS]");
        System.out.println("[ENTER SORT]:");
        System.out.println(Arrays.toString(TMSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final TMSort sort = TMSort.toSort(sortType);
        @NotNull final ProjectDisplayListRequest request = new ProjectDisplayListRequest(getToken(), sort);
        @NotNull final ProjectDisplayListResponse response = getProjectEndpoint().getAllProjects(request);
        @Nullable final List<ProjectDTO> projects = response.getProjects();
        renderProjects(projects);
    }

}
