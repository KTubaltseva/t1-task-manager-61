package ru.t1.ktubaltseva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.ktubaltseva.tm.dto.response.task.TaskUpdateByIdResponse;
import ru.t1.ktubaltseva.tm.event.ConsoleEvent;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdListener extends AbstractTaskListener {

    @NotNull
    private final String NAME = "task-update-by-id";

    @NotNull
    private final String DESC = "Update task by Id.";

    @NotNull
    @Override
    public String getCommandName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.getCommandName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken(), id, name, description);
        @NotNull final TaskUpdateByIdResponse response = getTaskEndpoint().updateTaskById(request);
        @Nullable final TaskDTO task = response.getTask();
        displayTask(task);
    }

}
