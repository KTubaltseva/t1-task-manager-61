package ru.t1.ktubaltseva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.t1.ktubaltseva.tm.component.Bootstrap;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfig;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class ClientApp {

    public static void main(@Nullable final String[] args) throws AbstractException, NoSuchAlgorithmException, IOException {
        @NotNull final AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start(args);
        context.registerShutdownHook();
    }

}